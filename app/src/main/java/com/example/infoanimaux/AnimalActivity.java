package com.example.infoanimaux;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);


        if(getIntent().getSerializableExtra("Animal") != null){
            Animal animal = (Animal) getIntent().getSerializableExtra("Animal");

            ImageView imageView_Animal = findViewById(R.id.imageView_Animal);

            //Display Image
            String uri = "@drawable/"+animal.getImgFile();
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imageView_Animal.setImageDrawable(res);


            TextView txtView_Name = findViewById(R.id.txtView_Name);
            final String AnimalName = getIntent().getSerializableExtra("AnimalName").toString();
            txtView_Name.setText(AnimalName);


            TextView txtView_hightestLifespan = findViewById(R.id.txtView_hightestLifespan);
            txtView_hightestLifespan.setText(animal.getStrHightestLifespan());

            TextView txtView_gestationPeriod = findViewById(R.id.txtView_gestationPeriod);
            txtView_gestationPeriod.setText(animal.getStrGestationPeriod());

            TextView txtView_birthWeight = findViewById(R.id.txtView_birthWeight);
            txtView_birthWeight.setText(animal.getStrBirthWeight());

            TextView txtView_adultWeight = findViewById(R.id.txtView_adultWeight);
            txtView_adultWeight.setText(animal.getStrAdultWeight());

            final EditText edtText_conservationStatus = findViewById(R.id.edtText_conservationStatus);
            edtText_conservationStatus.setText(animal.getConservationStatus());

            Button btn_save = findViewById(R.id.btn_save);
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnimalList.getAnimal(AnimalName).setConservationStatus(edtText_conservationStatus.getText().toString());
                }
            });



        }


    }
}
