package com.example.infoanimaux;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.ViewGroup;


public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        final RecyclerView rv =  findViewById(R.id.recycler_view);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new CustomRecyclerAdapter());


    }

    public class CustomRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new ViewHolder(getLayoutInflater().inflate(R.layout.layout_list_item, parent, false)));
        }

        @Override
        //On lie notre recycler view avec notre animalList pour recuperer le nom de l'animal
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.bindModel(AnimalList.getNameArray()[position]);
        }

        @Override
        public int getItemCount() {
            return(AnimalList.getNameArray().length);
        }



    }
}
