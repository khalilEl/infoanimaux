package com.example.infoanimaux;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

class ViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    TextView label;
    ImageView icon;

    ViewHolder(View row) {
        super(row);
        label =  row.findViewById(R.id.textView_Animal_name);
        icon =  row.findViewById(R.id.imageView_Animal);
        this.itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        
        final String item = label.getText().toString();
        //Envoyer les infomations a Animal Activity
        Intent intent = new Intent(this.itemView.getContext(), AnimalActivity.class);
        intent.putExtra("Animal",AnimalList.getAnimal(item));
        intent.putExtra("AnimalName", item);
        this.itemView.getContext().startActivity(intent);

    }

    void bindModel(String item) {
        label.setText(item);
        Animal animal = AnimalList.getAnimal(item);
        icon.setImageResource(this.itemView.getContext().getResources().getIdentifier(animal.getImgFile(), "drawable", this.itemView.getContext().getPackageName()));

    }

}
